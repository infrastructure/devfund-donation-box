#!/bin/bash
version="1.2.2"

# Collect static files
function collectstatic() {
  # Create directories
  if [ -d dist/fonts ]
  then
    echo "dist/fonts directory exists"
  else
    mkdir dist/fonts
    echo "dist/fonts directory created"
  fi

  if [ -d dist/js ]
  then
    echo "dist/js directory exists"
  else
    mkdir dist/js
    echo "dist/js directory created"
  fi

  # Copy files
  cp -R assets_shared/assets/fonts/* dist/fonts
  cp -R js/* dist/js

  # Rename files
  mv dist/js/app.js dist/js/donation-box.js
}

# Build component donation-box
function donationbox() {
  cat <(echo "<style>"; cat css/custom.min.css; echo "</style>") main-donation-box.html > dist/donation-box.html
  sed -i '1s/^/<!-- devfund-donation-box v='"$version"' -->\n/' dist/donation-box.html
  echo "Build component dist/donation-box.html completed"
}

# Build component donation-box-message
function donationboxmessage() {
  cat <(echo "<style>"; cat css/custom.min.css; echo "</style>") main-donation-box-message.html > dist/donation-box-message.html
  sed -i '1s/^/<!-- devfund-donation-box v='"$version"' -->\n/' dist/donation-box-message.html
  echo "Build component dist/donation-box-message.html completed"
}

# Init functions
function init() {
  collectstatic
  donationbox
  donationboxmessage
}

init
