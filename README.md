# Devfund Donation Box

Devfund Donation Box is a component to initialize donations to Blender Development Fund on internal and external websites.

## Installation

### Cloud
You can include Devfund Donation Box component directly from Blender's web infrastructure in your project.

- Include the following JavaScript plugin in your markup before the closing `<body>` tag: `<script src="https://fund.blender.org/static/fetch-donation-box.js"></script>`

### Self-hosting
Alternatively, you can self-host the Devfund Donation Box component. Initialize the library as a Git submodule in your project, following these steps:

1. Run the following command to add the library as a submodule to the root directory of your project:
`git submodule add https://projects.blender.org/infrastructure/devfund-donation-box.git`
2. Run the following command to add the library as a submodule:
`git submodule init`
3. Include the following JavaScript plugin in your markup before the closing `<body>` tag:
  `<script src="devfund-donation-box/dist/fetch-donation-box.js"></script>`

## Usage
Use the component in your project, following these steps:

1. Call the following function by passing in the options object with required settings as an argument in your JavaScript:
```
fetchDonationBox({
  jsUrl: 'https://fund.blender.org/static/js/donation-box.js',
  selector: '#js-donation-box',
  url: 'https://fund.blender.org/static/donation-box.html'
});
```

2. Add the following HTML code in your markup with the custom selector where you want the component to appear. For example, within `.col-6`:
```
<div class="col-6">
  <div id="js-donation-box"></div>
</div>
```

### Options
Customize the component's display and settings with the following options:

| Name | Type | Description | Others
| --- | --- | --- | --- |
| `bgGlossy` | boolean | Sets the component background to glossy, applying a 'frosted glass' effect. | _optional_ |
| `darkMode` | boolean | Sets the component colours to dark mode. | _optional_ |
| `jsOnly` | boolean | Activates JavaScript-only mode appending the component to the `<body>` tag. Useful for environments, where injecting scripts is only possible in `<head>`, or template markup is not editable. If `jsOnly` is true, `selector` option is ignored. | _optional_ |
| `jsUrl` | string | Sets the `donation-box.js` file path accordingly to installation type. | _required_ |
| `selector` | string | Sets the item's selector to append the component to. | _optional_ |
| `url` | string | Sets the component's file path accordingly to installation type. Valid component options are `donation-box.html` and `donation-box-message.html`. | _required_ |


## Build
Devfund Donation Box uses npm scripts to build component styles, and a Bash script to package styles, markup and JavaScript to a single HTML component as its build system.
- Work on the `sass/custom.css`, `main-donation-box.html`, `main-donation-box-message.html`, and `js/app.js` source files respectively.
- Install npm scripts by running `npm install` in the submodule root directory. Build styles with `npm run-script css` or `npm run-script watch`.
- Build the HTML component donation boxes, and collect static assets by running `./build.sh` in the submodule root directory.

## Deploy
TODO: optionally add instructions on Devfund Donation Box's deployment to Blender's web infrastructure

## Roadmap
- Update README

## Version
1.0.10
