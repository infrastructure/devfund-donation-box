module.exports = {
  plugins: [
    require('autoprefixer')(),
    require('postcss-prefix-selector')({
      exclude: ['.dfdb', '.dfdb.is-bg-glossy', '.dfdb.is-bg-glossy .box', '.dfdb.is-bg-glossy.is-dark-mode', '.dfdb.is-dark-mode', '.dfdb.smaller'],
      prefix: '.dfdb',
      transform: function(prefix, selector, prefixedSelector) {
        var found = selector.match(/^\s*:root(\s+\S.*)?$/)
        if (found) {
          return found[1] === undefined ? prefix : `${prefix}${found[1]}`
        }
        return prefixedSelector
      }
    })
  ]
}
