// Fetch script should also include a version otherwise
// it won't be possible to updated it once it's cached in someone's browser.
const version = '1.0.10';
// Necessary in case the page wasn't already modified to include the fetch script:
const fetchScriptUrl = 'https://fund.blender.org/static/fetch-donation-box.js?v=' + version;
const newScript = document.createElement('script');
newScript.setAttribute('src', fetchScriptUrl);
newScript.async = false;
document.body.appendChild(newScript);

// Necessary in case the page doesn't already contain a container for the donation box:
const div = document.createElement('div');
const testElement = document.createElement('div');
testElement.id = 'js-donation-box';
div.appendChild(testElement);

const jsUrl = 'https://fund.blender.org/static/js/donation-box.js';
const url = 'https://fund.blender.org/static/donation-box-message.html';

/* On different pages a different element might to be used
 * for appending the donation box to, however simply using
 * document.body works on most of *.blender.org websites.
 */
const appendTo = document.body;
appendTo.appendChild(div);

/* `fetchDonationBox` will be available after the script we just added to the page loads.
 *
 * Under "normal" circumstances `fetchDonationBox` should be called on DOMContentLoaded,
 * unless `fetch-donation-box.js` is included with async=true
 */
newScript.addEventListener('load', function() {
  fetchDonationBox({
    jsUrl: jsUrl,
    selector: '#js-donation-box',
    url: url
  });
});
