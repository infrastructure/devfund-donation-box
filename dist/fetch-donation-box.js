function fetchDonationBox(options) {
  // Get settings fetch
  var settings = options;
  var version = '1.0.10';

  // Create function checkCookie
  function checkCookie(name) {
    var cookies = document.cookie.split(';');

    for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i].trim();

      if (cookie.indexOf(name + '=') === 0) {
        return true;
      }
    }

    return false;
  }

  // Check if donation box message is dismissed either manually or by donation ('donatedAt' cookie), and if settings.url is set to 'donation-box-message.html'
  if ((checkCookie('donationBoxMessageIsDismissed') || checkCookie('donatedAt')) && settings.url.includes('donation-box-message.html')) {
    return;
  }

  fetch(settings.url + '?v=' + version)
    .then(function(response) { // Handle the response
      return response.text(); // Extract the text from the response
    })
    .then(function(body) { // Handle the extracted data
      // Init variable donationBox
      var donationBox;

      // Check if jsOnly mode is active
      if (settings.jsOnly == true) {
        // Create element donationBox
        donationBox = document.createElement('div');

        // TODO: create function named?
        donationBox.innerHTML = body; // Insert the text into the #js-donation-box element

        // Append donationBox to body
        document.body.appendChild(donationBox);
      } else {
        donationBox = document.querySelector(settings.selector);
        donationBox.innerHTML = body;
      }

      // Create variable dfdb
      var dfdb = document.querySelector('.dfdb');

      // Check if bg is glossy
      if (settings.bgGlossy == true) {
        dfdb.classList.add('is-bg-glossy');
      }

      // Check if is dark mode
      if (settings.darkMode == true) {
        // Set dark mode
        dfdb.classList.add('is-dark-mode');
      }
    })
    .then(function() { // Insert the script separately, because <script> tags under innerHTML are not evaluated or executed
      var script = document.createElement('script');

      script.setAttribute('src', settings.jsUrl + '?v=' + version);
      document.body.appendChild(script);
    })
    .catch(function(error) { // Handle errors
      console.error(error); // Log the error message to the console
    });
}
