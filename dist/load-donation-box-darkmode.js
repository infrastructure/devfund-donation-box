fetchDonationBox({
    jsUrl: 'https://fund.blender.org/static/js/donation-box.js',
    jsOnly: true,
    bgGlossy: true,
    darkMode: true,
    url: 'https://fund.blender.org/static/donation-box-message.html'
});
