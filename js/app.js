(function() {
  // Create variables btns and inputs
  var dropdownCurrencySelect = document.querySelector('.js-dropdown-currency-select');

  var btnMessageDismiss = document.querySelectorAll('.js-btn-message-dismiss');

  var btnMonthlyDonate = document.querySelector('.js-btn-monthly-donate');
  var btnMonthlySelect = document.querySelectorAll('.js-btn-monthly-select');
  var btnMonthlySelectDefault = document.querySelector('.js-btn-monthly-select-default');

  var btnOneTimeDonate = document.querySelector('.js-btn-one-time-donate');
  var btnOneTimeSelect = document.querySelectorAll('.js-btn-one-time-select');
  var btnOneTimeSelectDefault = document.querySelector('.js-btn-one-time-select-default');

  var btnTabbed = document.querySelectorAll('.js-btn-tabbed');

  var inputOneTimeDonate = document.querySelector('.js-input-one-time-donate');

  // Create variables components
  var donationBox = document.querySelector('.js-donation-box');
  var donationBoxMessage = document.querySelector('.js-donation-box-message');

  var linkBadgeLg30y = document.querySelector('.js-link-badge-lg-30y');

  var monthlyImgBadge = document.querySelectorAll('.js-monthly-img-badge');
  var monthlyLevel = document.querySelectorAll('.js-monthly-level');

  var oneTimeUrlBase = 'https://fund.blender.org/express-checkout/';

  var tabbedPanel = document.querySelectorAll('.js-tabbed-panel');

  var valueAmount = document.querySelectorAll('.js-value-amount');
  var valueSymbol = document.querySelectorAll('.js-value-symbol');

  // Create variables display
  var featureItem = document.querySelectorAll('.js-feature-item');
  var featureLink = document.querySelector('.js-feature-link');
  var featureLogo = document.querySelector('.js-feature-logo');
  var featureName = document.querySelector('.js-feature-name');

  var linkCorporateMemberships = document.querySelector('.js-link-corporate-memberships');

  // Create variable monthlyPlans objects
  var monthlyPlans = [
    {
      level: 'Bronze',
      link: false,
      logo: false,
      name: false,
      urlEUR: 'https://fund.blender.org/express-checkout/1/',
      urlUSD: 'https://fund.blender.org/express-checkout/2/',
      valueEUR: 5,
      valueUSD: 5
    },
    {
      level: 'Silver',
      link: false,
      logo: false,
      name: false,
      urlEUR: 'https://fund.blender.org/express-checkout/5/',
      urlUSD: 'https://fund.blender.org/express-checkout/6/',
      valueEUR: 10,
      valueUSD: 10
    },
    {
      level: 'Gold',
      link: false,
      logo: false,
      name: true,
      urlEUR: 'https://fund.blender.org/express-checkout/7/',
      urlUSD: 'https://fund.blender.org/express-checkout/8/',
      valueEUR: 25,
      valueUSD: 25
    },
    {
      level: 'Platinum',
      link: false,
      logo: false,
      name: true,
      urlEUR: 'https://fund.blender.org/express-checkout/9/',
      urlUSD: 'https://fund.blender.org/express-checkout/10/',
      valueEUR: 50,
      valueUSD: 50
    },
    {
      level: 'Titanium',
      link: true,
      logo: false,
      name: false,
      urlEUR: 'https://fund.blender.org/express-checkout/11/',
      urlUSD: 'https://fund.blender.org/express-checkout/12/',
      valueEUR: 100,
      valueUSD: 100
    },
    {
      level: 'Diamond',
      link: true,
      logo: false,
      name: false,
      urlEUR: 'https://fund.blender.org/express-checkout/15/',
      urlUSD: 'https://fund.blender.org/express-checkout/16/',
      valueEUR: 250,
      valueUSD: 250
    }
  ];

  // Create variable oneTimePlans object
  var oneTimePlans = [
    {
      valueEUR: 25,
      valueUSD: 25
    },
    {
      valueEUR: 50,
      valueUSD: 50
    },
    {
      valueEUR: 100,
      valueUSD: 100
    },
    {
      valueEUR: 150,
      valueUSD: 150
    },
    {
      valueEUR: 250,
      valueUSD: 250
    },
    {
      valueEUR: 500,
      valueUSD: 500
    }
  ];

  // Create function animateLinkBadgeLg30y
  function animateLinkBadgeLg30y() {
    // Check if linkBadgeLg30y exists
    if (linkBadgeLg30y == null) {

    } else {
      // Init animate on load
      linkBadgeLg30y.classList.add('animate__bounce');

      linkBadgeLg30y.addEventListener('animationend', function() {
        linkBadgeLg30y.classList.remove('animate__bounce');
      });

      // Create events mouse
      linkBadgeLg30y.addEventListener('mouseover', function() {
        this.classList.add('animate__bounce');
      });

      linkBadgeLg30y.addEventListener('mouseleave', function(){
        this.classList.remove('animate__bounce');
      });
    }
  }

  // Create function btnActive
  function btnActive(btnReset, btnActive) {
    // Reset btns
    btnReset.forEach(function (item) {
      item.classList.remove('active');
    });

    // Make item active
    btnActive.classList.add('active');
  }

  // Create function checkCookie
  function checkCookie(name) {
    var cookies = document.cookie.split(';');

    for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i].trim();

      if (cookie.indexOf(name + '=') === 0) {
        return true;
      }
    }

    return false;
  }

  // Create function currencyDisplayActive
  function currencyDisplayActive(currency) {
    // Change monthly value amounts
    valueAmount.forEach(function(item, index) {
      var valueGroup = item.getAttribute('data-value-group');

      // Init variables
      var amount;
      var plan;

      // Check if value is one-time (default is 'monthly')
      if (valueGroup == 'one-time') {
        plan = oneTimePlans[index - monthlyPlans.length];
      } else {
        plan = monthlyPlans[index];
      }

      // Check currency
      if (currency == 'eur') {
        amount = plan.valueEUR;
      } else {
        amount = plan.valueUSD;
      }

      item.textContent = amount;
    });

    // Change value symbols
    if (currency == 'eur') {
      valueSymbol.forEach(function(item) {
        item.textContent = '€';
      })
    } else {
      valueSymbol.forEach(function(item) {
        item.textContent = '$';
      })
    }
  }

  // Create function currencySelectActive
  function currencySelectActive(currency) {
    var btnMonthlySelectActive = document.querySelector('.js-btn-monthly-select.active');
    var btnOneTimeSelectActive = document.querySelector('.js-btn-one-time-select.active');

    donationBox.classList.remove('js-helper-currency-eur', 'js-helper-currency-usd');

    // Check currency active
    if (currency == 'eur') {
      donationBox.classList.add('js-helper-currency-eur');
      currencyDisplayActive('eur');
    } else {
      donationBox.classList.add('js-helper-currency-usd');
      currencyDisplayActive('usd');
    }

    // Reset select
    monthlySelectActive(btnMonthlySelectActive);
    oneTimeSelectActive(btnOneTimeSelectActive);
  }

  // Create function donationBoxMessageDismiss
  function donationBoxMessageDismiss(item) {
    var countExpirationDays = item.getAttribute('data-count-expiration-days');

    // Create function setCookieIsDismissed
    function setCookieIsDismissed(number) {
      document.cookie = "donationBoxMessageIsDismissed=" + Date.now() + "; domain=.blender.org; path=/; expires=" + new Date(Date.now() + number * 24 * 60 * 60 * 1000).toUTCString();
    }

    setCookieIsDismissed(countExpirationDays);

    // Reset donationBoxMessageShow
    donationBoxMessageShow();
  }

  // Create function donationBoxMessageShow
  function donationBoxMessageShow() {
    // Check if donationBoxMessage exists
    if (donationBoxMessage == null) {

    } else {
      // Check if donation box message is dismissed either manually or by donation ('donatedAt' cookie)
      if (checkCookie('donationBoxMessageIsDismissed') || checkCookie('donatedAt')) {
        // Hide donationBoxMessage
        donationBoxMessage.classList.remove('show');
        donationBoxMessage.classList.add('h-0'); // Make donationBoxMessage hidden while preserving fade
        donationBoxMessage.classList.add('p-0');
      } else {
        // Show donationBoxMessage
        donationBoxMessage.classList.add('show');
        donationBoxMessage.classList.remove('h-0');
        donationBoxMessage.classList.remove('p-0');
      }
    }
  }

  // Create function monthlySelectActive
  function monthlySelectActive(item) {
    var index = Array.from(item.parentNode.children).indexOf(item);
    var monthlyPlan = monthlyPlans[index];

    // Create function featureItemShow
    function featureItemShow(item) {
      item.classList.add('show');
      item.classList.remove('h-0');
    }

    btnActive(btnMonthlySelect, item);

    // Reset display features
    featureItem.forEach(function(item) {
      item.classList.add('h-0');
      item.classList.remove('show');
    });

    // Check if feature exists
    if (monthlyPlan.link) {
      featureItemShow(featureLink);
    }

    if (monthlyPlan.logo) {
      featureItemShow(featureLogo);
    }

    if (monthlyPlan.name) {
      featureItemShow(featureName);
    }

    // Reset monthly img badges
    monthlyImgBadge.forEach(function(item) {
      item.classList.remove('z-3');
    });

    // Change monthly img badge
    monthlyImgBadge[index].classList.add('z-3');

    // Change monthly level
    monthlyLevel.forEach(function(item) {
      item.textContent = monthlyPlan.level;
    });

    // Change btn monthly donate url
    if (donationBox.classList.contains('js-helper-currency-eur')) {
      btnMonthlyDonate.href = monthlyPlan.urlEUR;
    } else {
      btnMonthlyDonate.href = monthlyPlan.urlUSD;
    }
  }

  // Create function oneTimeSelectActive
  function oneTimeSelectActive(item) {
    var index = Array.from(item.parentNode.children).indexOf(item);
    var oneTimePlan = oneTimePlans[index];

    // Init variables value
    var valueAmount;
    var valueCurrency;
    var valueSymbol;

    // Check currency active
    if (donationBox.classList.contains('js-helper-currency-eur')) {
      valueAmount = Number(oneTimePlan.valueEUR);
      valueCurrency = 'eur';
      valueSymbol = '€';
    } else {
      valueAmount = Number(oneTimePlan.valueUSD);
      valueCurrency = 'usd';
      valueSymbol = '$';
    }

    // Create function oneTimeUrlChange
    function oneTimeUrlChange(value) {
      const valueInCents = Number(value).toFixed(2).replace('.', '');
      btnOneTimeDonate.href = oneTimeUrlBase + '?valueAmount=' + valueInCents + '&valueCurrency=' + valueCurrency;
    }

    // Create function oneTimeInputCustom
    function oneTimeInputCustom() {
      inputOneTimeDonate.addEventListener('input', function() {
        if (!inputOneTimeDonate.reportValidity()) {
          return;
        }
        var value = this.value;

        // Change btn one time donate url
        oneTimeUrlChange(value);
      });
    }

    btnActive(btnOneTimeSelect, item);

    // Change btn one time donate url
    oneTimeUrlChange(valueAmount);

    // Change input one time value
    inputOneTimeDonate.value = valueAmount;

    // Init function oneTimeInputCustom
    oneTimeInputCustom();
  }

  // Create function tabbedPanelActive
  function tabbedPanelActive(item) {
    var index = Array.from(item.parentNode.children).indexOf(item);

    // Reset btns tabbed
    btnTabbed.forEach(function(item) {
      item.classList.remove('active');
    });

    // Make item active
    item.classList.add('active');

    // Reset tabbed panels
    tabbedPanel.forEach(function(item) {
      item.classList.add('h-0');
      item.classList.add('overflow-hidden'); // Class 'overflow-hidden' is managed by JavaScript to enable magin-top negative
      item.classList.remove('show');
    });

    // Show tabbed panel active
    tabbedPanel[index].classList.remove('h-0');
    tabbedPanel[index].classList.remove('overflow-hidden');
    tabbedPanel[index].classList.add('show');

    // Check if first btn tabbed is clicked
    if (index == 0) {
      // Toggle linkCorporateMemberships
      linkCorporateMemberships.classList.remove('d-none');
    } else {
      linkCorporateMemberships.classList.add('d-none');
    }
  }

  // Init function animateLinkBadgeLg30y
  // Disable function animateLinkBadgeLg30y for now
  // animateLinkBadgeLg30y();

  // Create event click btn message dismiss
  btnMessageDismiss.forEach(function(item) {
    item.addEventListener('click', function() {
      donationBoxMessageDismiss(item);
    });
  })

  // Show donation box message
  donationBoxMessageShow();

  // Set monthly select default
  monthlySelectActive(btnMonthlySelectDefault);

  // Create event click btn monthly event
  btnMonthlySelect.forEach(function(item) {
    item.addEventListener('click', function() {
      monthlySelectActive(item);
    });
  });

  // Set one time select default
  oneTimeSelectActive(btnOneTimeSelectDefault);

  // Create event click btn one time event
  btnOneTimeSelect.forEach(function(item) {
    item.addEventListener('click', function() {
      oneTimeSelectActive(item);
    });
  });

  // Set currency select default
  currencySelectActive('eur');

  // Create event dropdown currency select
  dropdownCurrencySelect.addEventListener('change', function(event) {
    var value = event.target.value;

    currencySelectActive(value);
  });

  // Create event click btn tabbed
  btnTabbed.forEach(function(item) {
    item.addEventListener('click', function() {
      tabbedPanelActive(item);
    });
  });
})();
